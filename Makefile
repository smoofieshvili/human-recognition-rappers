DOCKER=nvidia-docker
PORT=6006
JUPYTER_PORT=8888
TRAIN_GPU=1
EVAL_GPU=1
VOLUME_PATH=$(PWD)/volume

IMAGE=pawelkobojek/biometrics
#IMAGE=gcr.io/tensorflow/tensorflow:latest-gpu
CONTAINER_NAME=biometrics

build:
	$(DOCKER) build $(ADDITIONAL_ARGS) -t $(IMAGE) .

build-gpu: build

run:
	$(DOCKER) run \
	--name=$(CONTAINER_NAME) \
	--rm \
	-p $(PORT):6006 \
	-it \
	-v $(VOLUME_PATH):/data \
	-v $(HOME)/.keras:/root/.keras \
	$(IMAGE) \
	$(TRAIN_GPU) \
	$(EVAL_GPU)

run-gpu:
	$(DOCKER) run \
	--runtime=nvidia \
	--name=$(CONTAINER_NAME) \
	--rm \
	-p $(PORT):6006 \
	-it \
	-v $(VOLUME_PATH):/data \
	-v $(HOME)/.keras:/root/.keras \
	$(IMAGE) \
	$(TRAIN_GPU) \
	$(EVAL_GPU)

bash:
	$(DOCKER) run \
	--entrypoint=/bin/bash \
	--name=$(CONTAINER_NAME) \
	--rm \
	-p $(PORT):6006 \
	-it \
	-v $(VOLUME_PATH):/data \
	-v $(HOME)/.keras:/root/.keras \
	$(IMAGE)

bash-gpu:
	$(DOCKER) run \
	--runtime=nvidia \
	--entrypoint=/bin/bash \
	--name=$(CONTAINER_NAME) \
	--rm \
	-p $(PORT):6006 \
	-it \
	-v $(VOLUME_PATH):/data \
	-v $(HOME)/.keras:/root/.keras \
	$(IMAGE)

test:
	$(DOCKER) run \
	--entrypoint=python \
	--rm \
	-it \
	-v $(VOLUME_PATH):/data \
	-v $(HOME)/.keras:/root/.keras \
	$(IMAGE) \
	-m unittest discover

test-gpu:
	$(DOCKER) run \
	--runtime=nvidia \
	--entrypoint=python \
	--rm \
	-it \
	-v $(VOLUME_PATH):/data \
	-v $(HOME)/.keras:/root/.keras \
	$(IMAGE) \
	-m unittest discover

notebooks: build
	$(DOCKER) run \
	--entrypoint=jupyter \
	--name=$(CONTAINER_NAME) \
	--rm \
	-p $(JUPYTER_PORT):8888 \
	-it \
	-v $(PWD)/notebooks/:/notebooks/ \
	-v $(HOME)/.keras:/root/.keras \
	$(IMAGE) \
	notebook --allow-root /notebooks/

notebooks-gpu: build-gpu
	$(DOCKER) run \
	--runtime=nvidia \
	--entrypoint=jupyter \
	--name=$(CONTAINER_NAME) \
	--rm \
	-p $(JUPYTER_PORT):8888 \
	-it \
	-v $(PWD)/notebooks/:/notebooks/ \
	-v $(HOME)/.keras:/root/.keras \
	$(IMAGE) \
	notebook --allow-root /notebooks/

clean:
	rm -rf volume/

clear: clean

clean-gpu: clean
clear-gpu: clear

all: build run
all-gpu: build-gpu run-gpu

.PHONY: build build-gpu run run-gpu bash bash-gpu test test-gpu clean clean-gpu clear clear-gpu notebooks notebooks-gpu
