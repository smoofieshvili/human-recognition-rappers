FROM tensorflow/tensorflow:latest-gpu-py3

WORKDIR /face

ENV PYTHONPATH=/face

EXPOSE 6006

COPY requirements.txt requirements.txt

RUN pip install -r requirements.txt

COPY run.sh .
COPY ./test/ ./test/
COPY ./biometrics_face/ ./biometrics_face/
COPY weights.h5 .

ENTRYPOINT ["./run.sh"]
