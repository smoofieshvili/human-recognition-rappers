#!/usr/bin/env python
import argparse
from keras.preprocessing.image import ImageDataGenerator, array_to_img, img_to_array, load_img

from keras import utils
from keras.datasets import mnist, cifar10

#from biometrics_face 
import model_builder

defaults = {
	'num_classes': 4,
	'epochs': 100,
	'batch_size': 16,
	'use_cnn': True,
}


def main(args):

	datagen = ImageDataGenerator(validation_split=0.2, rotation_range=40,
        width_shift_range=0.2,
        height_shift_range=0.2,
        shear_range=0.2,
        zoom_range=0.2,
        horizontal_flip=True)
	
	data_train = datagen.flow_from_directory("biometrics_face/train", subset="training", target_size=(224,224), batch_size=args.batch_size, class_mode='categorical')
	data_validation = datagen.flow_from_directory("biometrics_face/train", subset='validation', target_size=(224,224), batch_size=args.batch_size, class_mode='categorical')

   # if args.use_cnn:
	#	 train_images = train_images.reshape(60000, 28, 28, 1)
	 #	 test_images = test_images.reshape(10000, 28, 28, 1)
	#else:
	#	 train_images = train_images.reshape(60000, 784)
	#	 test_images = test_images.reshape(10000, 784)

	#train_labels = utils.to_categorical(train_labels, args.num_classes)
	#test_labels = utils.to_categorical(test_labels, args.num_classes)

	model = model_builder.build_task1(input_shape=(224, 224, 3), num_classes=4)

	model.load_weights('weights.h5')

	model.compile(
		optimizer='adam',
		loss='categorical_crossentropy',
		metrics=['accuracy'])

	model.fit_generator(
		data_train,
		steps_per_epoch=7997*0.8 // args.batch_size,
		epochs=args.epochs,
		validation_data=data_validation,
		validation_steps = 7997*0.2 // args.batch_size,
	)

	model.save_weights('nenenene.h5')

	print('If you see this text, everything is probably okay.')


def parse_arguments():
	parser = argparse.ArgumentParser(
		description='Trains either MLP or CNN model on MNIST dataset')
	parser.add_argument(
		'--num-classes',
		help='Number of classes in the classification problem.',
		type=int,
		default=defaults['num_classes'])
	parser.add_argument(
		'--epochs',
		help='Number of epochs to train.',
		type=int,
		default=defaults['epochs'])
	parser.add_argument(
		'--batch-size',
		help='Batch size',
		type=int,
		default=defaults['batch_size'])
	parser.add_argument(
		'--use-cnn',
		help='True if CNN should be used, False for MLP',
		type=bool,
		default=defaults['use_cnn'])

	return parser.parse_args()


if __name__ == '__main__':
	main(parse_arguments())
